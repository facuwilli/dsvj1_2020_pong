#pragma once
#include "..\..\gameplay\include\structBall.h"
#include "..\..\gameplay\include\structPad.h"

namespace GAME
{
	namespace BALL
	{
		void MoveBall(Ball& ball);
		void BallBouncing(Ball& ball, int screenWidth, int screenHeight, Pad& pad1, Pad& pad2);
		void BallMovement(Ball& ball, Pad& pad1, Pad& pad2, int screenWidth, int screenHeight);
		void BallCollision(Ball& ball, Pad& pad);
		Ball setBall(float xPos, float yPos, float xSpeed, float ySpeed, Color color, float radius);
		void PowerUpsManagments(Pad& pad, bool typePad);
		void ResetPowers(Pad& pad, bool resetType);
	}
}


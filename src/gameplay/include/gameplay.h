#pragma once
#include "..\..\gameplay\include\menus.h"
#include "..\..\gameplay\include\structBall.h"
#include "..\..\gameplay\include\structPad.h"
#include "..\..\gameplay\include\score.h"
#include "..\..\gameplay\include\pad.h"
#include "..\..\gameplay\include\ball.h"

namespace GAME
{
	const int screenWidth = 800;
	const int screenHeight = 450;

	extern Pad pad1;
	extern Pad pad2;
	extern Ball ball;

	void InitGame();
	void InitMatch(bool& exitMatch, bool& exitGame);
	void Input(bool& exitMatch);
	void Update();
	void Output();
	void DeInit();
	void run(bool& exitGame);
}
#pragma once
#include "..\..\gameplay\include\structPad.h"

namespace GAME
{
	namespace PAD
	{
		extern int moveUpP1;
		extern int moveDownP1;
		extern int moveUpP2;
		extern int moveDownP2;

		bool CheckedHigherLimiteReached(short HigherLimite, float yPosition, short padHeight);
		bool CheckedLowerLimiteReached(short LowerLimite, float yPosition);
		void MovePadVerticaly(int key, Pad& padSelected, bool moveUp);
		void PadsMovement(Pad& pad1, Pad& pad2, int screenHeight);
		Pad SetPad(float x, float y, float width, float height, Color color, float moveSpeed, EnumColors _color, PowerUps _powerUp);
		void UpdateSurfacesPos(Pad& pad, float value);
		Rectangle SetSurface(float _X, float _Y, float _Width, float _Height);
		void ChangePadCOlor(Pad& padSelected);
		int SelectAKey();
		void ChangeAMovementKey(short select);
		void ShowControlsOnScreen();
		void BiggerPaddle(Pad& pad);
		void PlusSpeed(Pad& pad);
		void ReverseControls(bool reverseType);
	}
}
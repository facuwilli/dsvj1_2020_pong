#pragma once
#include "raylib.h"
enum class EnumColors { green, brown, skyblue, violet, gold, red};
enum class PowerUps { none, bigPad, plusSpeed };

struct Pad 
{
	Rectangle pad;
	Color color;
	float moveSpeed;
	EnumColors _color;
	Rectangle higherSurface;
	Rectangle lowerSurface;
	PowerUps powerUp = PowerUps::none;
};
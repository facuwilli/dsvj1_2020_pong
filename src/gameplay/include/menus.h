#pragma once
#include "raylib.h"

namespace GAME
{
	namespace MENU
	{
		enum class Menu { play, controls, credits, exit, resume, goBack, keyUpPj1, keyDownPj1, keyUpPj2, keyDownPj2 };
		extern Menu option;

		extern bool onLoop;

		extern Color cJugar;
		extern Color cControles;
		extern Color cCreditos;
		extern Color cSalir;
		extern Color cReanudar;
		extern Color cVolver;
		extern Color cKeyUp1;
		extern Color cKeyUp2;
		extern Color cKeyDown1;
		extern Color cKeyDown2;
		extern Color cNewKey;

		void ControlsConfig(bool& exitMatch);
		void HighlightOptionSelected();
		void InicialMenu(bool& exitMatch, bool& exitGame);
		void PauseMenu(bool& exitMatch);
		void ShowCredits();
	}
}
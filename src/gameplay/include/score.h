#pragma once
namespace GAME
{
	namespace SCORE
	{
		extern char score1[2];
		extern char score2[2];

		void DrawMarker();
		void ResetMarker();
		void IncreaseMarker(short markerSelected);
		bool WiningScoreReached();
		void ChangeNeededScoreToWin();
	}
}
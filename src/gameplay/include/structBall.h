#pragma once
#include "raylib.h"

struct Ball
{
	Vector2 ballPosition;
	Vector2 initPosition;
	Vector2 speed;
	float radius;
	Color color;			
};
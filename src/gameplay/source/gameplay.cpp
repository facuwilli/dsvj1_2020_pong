#include "..\..\gameplay\include\gameplay.h"
using namespace GAME;
using namespace BALL;
using namespace PAD;
using namespace SCORE;
using namespace MENU;

namespace GAME
{
	Pad pad1;
	Pad pad2;
	Ball ball;

	namespace BALL
	{
		void ResetPowers(Pad& pad, bool resetType)
		{
			switch (resetType)
			{
			case true:
				pad = SetPad(40.0f, 450 / 2 - pad.pad.height / 2, 30.0f, 100.0f, GREEN, 4.5f, EnumColors::green, PowerUps::none);
				pad.higherSurface = SetSurface(pad.pad.x, pad.pad.y - 2, (float)(pad.pad.width - 0.25), 3);
				pad.lowerSurface = SetSurface(pad.pad.x, (pad.pad.y + pad.pad.height + 2), (float)(pad.pad.width - 0.50), 2);
				break;
			case false:
				pad = SetPad(740.0f, 450 / 2 - pad.pad.height / 2, 30.0f, 100.0f, GREEN, 4.5f, EnumColors::green, PowerUps::none);
				pad.higherSurface = SetSurface(pad.pad.x, pad.pad.y - 2, (float)(pad.pad.width - 0.25), 3);
				pad.lowerSurface = SetSurface(pad.pad.x, (pad.pad.y + pad.pad.height + 2), (float)(pad.pad.width - 0.50), 2);
				break;
			default:
				break;
			}
		}

		void PowerUpsManagments(Pad& pad, bool typePad)
		{
			if (pad.powerUp != PowerUps::plusSpeed)
			{
				switch (pad.powerUp)
				{
				case PowerUps::none:
					pad.powerUp = PowerUps::bigPad;
					break;
				case PowerUps::bigPad:
					pad.powerUp = PowerUps::plusSpeed;
					BiggerPaddle(pad);
					break;
				case PowerUps::plusSpeed:
					PlusSpeed(pad);
					break;
				default:
					break;
				}
			}
		}

		void BallBouncing(Ball& ball, int screenWidth, int screenHeight, Pad& pad1, Pad& pad2)
		{
			if (ball.ballPosition.x >= screenWidth || ball.ballPosition.x <= 0)
			{
				if (ball.ballPosition.x >= screenWidth)
				{
					IncreaseMarker(1);
				}
				else
				{
					IncreaseMarker(2);
				}

				ball.ballPosition = ball.initPosition;
				ball.speed.x *= -1;
			}

			if (ball.ballPosition.y >= screenHeight || ball.ballPosition.y <= 0)
			{
				ball.speed.y *= -1;
			}
		}

		void BallCollision(Ball& ball, Pad& pad)
		{
			if (CheckCollisionCircleRec(ball.ballPosition, ball.radius, pad.pad))
			{
				ball.speed.x *= -1;
			}

			if (CheckCollisionCircleRec(ball.ballPosition, ball.radius, pad.higherSurface))
			{
				ball.speed.y *= -1;
			}

			if (CheckCollisionCircleRec(ball.ballPosition, ball.radius, pad.lowerSurface))
			{
				ball.speed.y *= -1;
			}
		}

		void MoveBall(Ball& ball)
		{
			ball.ballPosition.x += ball.speed.x;
			ball.ballPosition.y += ball.speed.y;
		}

		void BallMovement(Ball& ball, Pad& pad1, Pad& pad2, int screenWidth, int screenHeight)
		{
			MoveBall(ball);
			BallCollision(ball, pad1);
			BallCollision(ball, pad2);
			BallBouncing(ball, screenWidth, screenHeight, pad1, pad2);
		}

		Ball setBall(float xPos, float yPos, float xSpeed, float ySpeed, Color color, float radius)
		{
			Ball ball;
			ball.initPosition.x = xPos;
			ball.initPosition.y = yPos;
			ball.ballPosition = ball.initPosition;
			ball.color = color;
			ball.radius = radius;
			ball.speed.x = xSpeed;
			ball.speed.y = ySpeed;

			return ball;
		}
	}

	namespace PAD
	{
		int moveUpP1 = KEY_W;
		int moveDownP1 = KEY_S;
		int moveUpP2 = KEY_UP;
		int moveDownP2 = KEY_DOWN;

		bool CheckedHigherLimiteReached(short HigherLimite, float yPosition, short padHeight)
		{
			return yPosition < HigherLimite - padHeight;
		}

		bool CheckedLowerLimiteReached(short LowerLimite, float yPosition)
		{
			return yPosition > LowerLimite;
		}

		void UpdateSurfacesPos(Pad& pad, float value)
		{
			pad.higherSurface.y += value;
			pad.lowerSurface.y += value;
		}

		void MovePadVerticaly(int key, Pad& padSelected, bool moveUp)
		{
			if (IsKeyDown(key))
			{
				switch (moveUp)
				{
				case true:
					padSelected.pad.y -= padSelected.moveSpeed;
					UpdateSurfacesPos(padSelected, -padSelected.moveSpeed);
					break;
				case false:
					padSelected.pad.y += padSelected.moveSpeed;
					UpdateSurfacesPos(padSelected, padSelected.moveSpeed);
					break;
				}
			}
		}

		void PadsMovement(Pad& pad1, Pad& pad2, int screenHeight)
		{
			if (CheckedLowerLimiteReached(0, pad1.pad.y))
			{
				MovePadVerticaly(moveUpP1, pad1, true);
			}

			if (CheckedHigherLimiteReached(screenHeight, pad1.pad.y, pad1.pad.height))
			{
				MovePadVerticaly(moveDownP1, pad1, false);
			}

			if (CheckedLowerLimiteReached(0, pad2.pad.y))
			{
				MovePadVerticaly(moveUpP2, pad2, true);
			}

			if (CheckedHigherLimiteReached(screenHeight, pad2.pad.y, pad2.pad.height))
			{
				MovePadVerticaly(moveDownP2, pad2, false);
			}
		}

		Pad SetPad(float x, float y, float width, float height, Color color, float moveSpeed, EnumColors _color, PowerUps _powerUp)
		{
			Pad _pad;
			_pad.pad.x = x;
			_pad.pad.y = y;
			_pad.pad.width = width;
			_pad.pad.height = height;
			_pad.color = color;
			_pad.moveSpeed = moveSpeed;
			_pad._color = _color;
			_pad.powerUp = _powerUp;

			return _pad;
		}

		Rectangle SetSurface(float _X, float _Y, float _Width, float _Height)
		{
			Rectangle rec;
			rec.x = _X;
			rec.y = _Y;
			rec.width = _Width;
			rec.height = _Height;

			return rec;
		}

		void ChangePadCOlor(Pad& padSelected)
		{
			switch (padSelected._color)
			{
			case EnumColors::green:
				padSelected._color = EnumColors::brown;
				padSelected.color = BROWN;
				break;
			case EnumColors::brown:
				padSelected._color = EnumColors::skyblue;
				padSelected.color = SKYBLUE;
				break;
			case EnumColors::skyblue:
				padSelected._color = EnumColors::violet;
				padSelected.color = VIOLET;
				break;
			case EnumColors::violet:
				padSelected._color = EnumColors::gold;
				padSelected.color = GOLD;
				break;
			case EnumColors::gold:
				padSelected._color = EnumColors::red;
				padSelected.color = RED;
				break;
			case EnumColors::red:
				padSelected._color = EnumColors::green;
				padSelected.color = GREEN;
				break;
			}
		}

		int SelectAKey()
		{
			int key = 0;

			while (IsKeyUp(KEY_SPACE))
			{
				BeginDrawing();

				ClearBackground(BLACK);

				DrawText("OPRIMA LA TECLA QUE DESEAS (menos espacio)", 150, 150, 20, BLUE);
				DrawText("Y LUEGO OPRIMA ESPACIO", 150, 200, 20, BLUE);

				EndDrawing();

				if (IsKeyUp(KEY_SPACE))
				{
					key = GetKeyPressed();
				}
			}

			return key;
		}

		void ChangeAMovementKey(short select)
		{
			switch (select)
			{
			case 1:
				moveUpP1 = SelectAKey();
				break;
			case 2:
				moveUpP2 = SelectAKey();
				break;
			case 3:
				moveDownP1 = SelectAKey();
				break;
			case 4:
				moveDownP2 = SelectAKey();
				break;
			}
		}

		void ShowControlsOnScreen()
		{
			DrawText("W", 100, 150, 20, LIGHTGRAY);
			DrawText("S", 100, 300, 20, LIGHTGRAY);
			DrawText("UP", 700, 150, 20, LIGHTGRAY);
			DrawText("DOWN", 700, 300, 20, LIGHTGRAY);
			DrawText("PAUSA(P)", 340, 30, 20, LIGHTGRAY);
			DrawText("COLOR(C)", 90, 30, 20, LIGHTGRAY);
			DrawText("COLOR(N)", 600, 30, 20, LIGHTGRAY);
		}

		void BiggerPaddle(Pad& pad)
		{
			pad.pad.height += 50;
			pad.pad.y = 450 / 2 - 150.0f / 2;
			pad.higherSurface = SetSurface(pad.pad.x, pad.pad.y - 2, (float)(pad.pad.width - 0.25), 3);
			pad.lowerSurface = SetSurface(pad.pad.x, (pad.pad.y + pad.pad.height + 2), (float)(pad.pad.width - 0.50), 3);
		}

		void PlusSpeed(Pad& pad)
		{
			pad.moveSpeed += 10;
		}

		void ReverseControls(bool reverseType)
		{
			switch (reverseType)
			{
			case true:
				moveUpP1 = KEY_S;
				moveDownP1 = KEY_W;
				break;
				moveUpP2 = KEY_DOWN;
				moveDownP2 = KEY_UP;
			case false:
				break;
			}
		}
	}

	namespace SCORE
	{
		char score1[2] = { 48, 48 };
		char score2[2] = { 48, 48 };

		void DrawMarker()
		{
			DrawText(score1, 250, 60, 75, RED);
			DrawText(score2, 450, 60, 75, RED);
		}

		void ResetMarker()
		{
			score1[0] = 48;
			score1[1] = 48;
			score2[0] = 48;
			score2[1] = 48;
		}

		void IncreaseMarker(short markerSelected)
		{
			switch (markerSelected)
			{
			case 1:
				if (score1[1] < 57)
				{
					score1[1] += 1;
				}
				else
				{
					score1[1] = 48;
					score1[0] += 1;
				}
				break;
			case 2:
				if (score2[1] < 57)
				{
					score2[1] += 1;
				}
				else
				{
					score2[1] = 48;
					score2[0] += 1;
				}
				break;
			default:
				break;
			}
		}

		bool WiningScoreReached()
		{
			return (score1[0] == 50 && score1[1] == 49) || (score2[0] == 50 && score2[1] == 49);
		}

		void ChangeNeededScoreToWin()
		{

		}
	}

	namespace MENU
	{
		Menu option = Menu::play;

		bool onLoop = true;

		Color cJugar = BLUE;
		Color cControles = BLUE;
		Color cCreditos = BLUE;
		Color cSalir = BLUE;
		Color cReanudar = BLUE;
		Color cVolver = BLUE;
		Color cKeyUp1 = BLUE;
		Color cKeyUp2 = BLUE;
		Color cKeyDown1 = BLUE;
		Color cKeyDown2 = BLUE;
		Color cNewKey = BLUE;

		void HighlightOptionSelected()
		{
			switch (option)
			{
			case Menu::play:
				cJugar = WHITE;
				break;
			case Menu::controls:
				cControles = WHITE;
				break;
			case Menu::credits:
				cCreditos = WHITE;
				break;
			case Menu::exit:
				cSalir = WHITE;
				break;
			case Menu::resume:
				cReanudar = WHITE;
				break;
			case Menu::goBack:
				cVolver = WHITE;
				break;
			case Menu::keyUpPj1:
				cKeyUp1 = WHITE;
				break;
			case Menu::keyDownPj1:
				cKeyDown1 = WHITE;
				break;
			case Menu::keyUpPj2:
				cKeyUp2 = WHITE;
				break;
			case Menu::keyDownPj2:
				cKeyDown2 = WHITE;
			default:
				break;
			}
		}

		void ControlsConfig(bool& exitMatch)
		{
			option = Menu::keyUpPj1;
			onLoop = true;

			while (onLoop)
			{
				BeginDrawing();

				ClearBackground(BLACK);

				HighlightOptionSelected();

				DrawText("Mover pad izquierdo arriba", 300, 100, 20, cKeyUp1);
				DrawText("Mover pad izquierdo abajo", 300, 150, 20, cKeyDown1);
				DrawText("Mover pad derecho arriba", 300, 200, 20, cKeyUp2);
				DrawText("Mover pad derecho abajo", 300, 250, 20, cKeyDown2);
				DrawText("VOLVER", 300, 300, 20, cVolver);

				EndDrawing();

				if (IsKeyPressed(KEY_UP))
				{
					switch (option)
					{
					case Menu::keyDownPj1:
						option = Menu::keyUpPj1;
						cKeyDown1 = BLUE;
						break;
					case Menu::keyUpPj2:
						option = Menu::keyDownPj1;
						cKeyUp2 = BLUE;
						break;
					case Menu::keyDownPj2:
						option = Menu::keyUpPj2;
						cKeyDown2 = BLUE;
						break;
					case Menu::goBack:
						option = Menu::keyDownPj2;
						cVolver = BLUE;
						break;
					default:
						break;
					}
				}
				else if (IsKeyPressed(KEY_DOWN))
				{
					switch (option)
					{
					case Menu::keyUpPj1:
						option = Menu::keyDownPj1;
						cKeyUp1 = BLUE;
						break;
					case Menu::keyDownPj1:
						option = Menu::keyUpPj2;
						cKeyDown1 = BLUE;
						break;
					case Menu::keyUpPj2:
						option = Menu::keyDownPj2;
						cKeyUp2 = BLUE;
						break;
					case Menu::keyDownPj2:
						option = Menu::goBack;
						cKeyDown2 = BLUE;
						break;
					default:
						break;
					}
				}
				else if (IsKeyPressed(KEY_ENTER))
				{
					switch (option)
					{
					case Menu::keyUpPj1:
						//ChangeAMovementKey(1);
						break;
					case Menu::keyDownPj1:
						//ChangeAMovementKey(2);
						break;
					case Menu::keyUpPj2:
						//ChangeAMovementKey(3);
						break;
					case Menu::keyDownPj2:
						//ChangeAMovementKey(4);
						break;
					case Menu::goBack:
						onLoop = false;
						exitMatch = true;
						cVolver = BLUE;
						break;
					default:
						break;
					}
				}
			}
		}

		void ShowCredits()
		{
			while (IsKeyUp(KEY_SPACE))
			{
				BeginDrawing();

				ClearBackground(BLACK);

				DrawText("JUEGO DESARROLLADO POR", 200, 75, 20, ORANGE);
				DrawText("FACUNDO WILLIAMS", 200, 135, 40, GOLD);
				DrawText("OPRIME ESPACIO PARA VOLVER", 200, 250, 20, ORANGE);

				EndDrawing();
			}
		}

		void InicialMenu(bool& exitMatch, bool& exitGame)
		{
			onLoop = true;
			option = Menu::play;

			while (onLoop)
			{
				BeginDrawing();

				ClearBackground(BLACK);

				HighlightOptionSelected();

				DrawText("JUGAR", 300, 100, 20, cJugar);
				DrawText("CONTROLES", 300, 150, 20, cControles);
				DrawText("CREDITOS", 300, 200, 20, cCreditos);
				DrawText("SALIR", 300, 250, 20, cSalir);

				EndDrawing();

				if (IsKeyPressed(KEY_UP))
				{
					switch (option)
					{
					case Menu::controls:
						cControles = BLUE;
						option = Menu::play;
						break;
					case Menu::credits:
						cCreditos = BLUE;
						option = Menu::controls;
						break;
					case Menu::exit:
						cSalir = BLUE;
						option = Menu::credits;
						break;
					default:
						break;
					}
				}
				else if (IsKeyPressed(KEY_DOWN))
				{
					switch (option)
					{
					case Menu::play:
						cJugar = BLUE;
						option = Menu::controls;
						break;
					case Menu::controls:
						cControles = BLUE;
						option = Menu::credits;
						break;
					case Menu::credits:
						cCreditos = BLUE;
						option = Menu::exit;
						break;
					default:
						break;
					}
				}
				else if (IsKeyPressed(KEY_ENTER))
				{
					switch (option)
					{
					case Menu::play:
						onLoop = false;
						exitMatch = false;
						break;
					case Menu::controls:
						cControles = BLUE;
						ControlsConfig(exitMatch);
						break;
					case Menu::credits:
						ShowCredits();
						break;
					case Menu::exit:
						onLoop = false;
						exitGame = true;
						exitMatch = true;
						break;
					default:
						break;
					}
				}
			}
		}

		void PauseMenu(bool& exitMatch)
		{
			onLoop = true;
			option = Menu::resume;

			while (onLoop)
			{
				BeginDrawing();

				ClearBackground(BLACK);

				HighlightOptionSelected();

				DrawText("REANUDAR", 300, 100, 30, cReanudar);
				DrawText("VOLVER MENU", 300, 150, 30, cVolver);

				EndDrawing();

				if (IsKeyPressed(KEY_UP))
				{
					cVolver = BLUE;
					option = Menu::resume;
				}
				else if (IsKeyPressed(KEY_DOWN))
				{
					cReanudar = BLUE;
					option = Menu::goBack;
				}
				else if (IsKeyPressed(KEY_ENTER))
				{
					switch (option)
					{
					case Menu::resume:
						onLoop = false;
						break;
					case Menu::goBack:
						onLoop = false;
						exitMatch = true;
						cVolver = BLUE;
						break;
					default:
						break;
					}
				}
			}
		}
	}

	void InitGame()
	{
		InitWindow(screenWidth, screenHeight, "PONG GAME");
		HideCursor();
		SetTargetFPS(60);
	}

	void InitMatch(bool& exitMatch, bool& exitGame)
	{
		InicialMenu(exitMatch, exitGame);

		ResetMarker();

		pad1 = SetPad(40.0f, screenHeight / 2 - 100.0f / 2, 30.0f, 100.0f, GREEN, 4.5f, EnumColors::green, PowerUps::none);
		pad2 = SetPad(740.0f, pad1.pad.y, pad1.pad.width, pad1.pad.height, pad1.color, pad1.moveSpeed, EnumColors::green, PowerUps::none);
		ball = setBall(screenWidth / 2, screenHeight / 2, 4.5f, 10, WHITE, 15);
		pad1.higherSurface = SetSurface(pad1.pad.x, pad1.pad.y - 2, (float)(pad1.pad.width - 0.25), 3);
		pad1.lowerSurface = SetSurface(pad1.pad.x, (pad1.pad.y + pad1.pad.height + 2), (float)(pad1.pad.width - 0.50), 2);
		pad2.higherSurface = SetSurface(pad2.pad.x, pad2.pad.y - 2, (float)(pad2.pad.width - 0.25), 3);
		pad2.lowerSurface = SetSurface(pad2.pad.x, (pad2.pad.y + pad2.pad.height + 2), (float)(pad2.pad.width - 0.50), 2);
	}

	void Input(bool& exitMatch)
	{
		if (IsKeyPressed(KEY_P))
		{
			PauseMenu(exitMatch);
		}
		else if (IsKeyPressed(KEY_C))
		{
			ChangePadCOlor(pad1);
		}
		else if (IsKeyPressed(KEY_N))
		{
			ChangePadCOlor(pad2);
		}
	}

	void Update()
	{
		PadsMovement(pad1, pad2, screenHeight);
		BallMovement(ball, pad1, pad2, screenWidth, screenHeight);
	}

	void Output()
	{
		BeginDrawing();

		ClearBackground(BLACK);

		DrawRectangle(pad1.pad.x, pad1.pad.y, pad1.pad.width, pad1.pad.height, pad1.color);
		DrawRectangle(pad2.pad.x, pad2.pad.y, pad2.pad.width, pad2.pad.height, pad2.color);
		DrawCircleV(ball.ballPosition, ball.radius, ball.color);
		ShowControlsOnScreen();
		DrawMarker();

		EndDrawing();
	}

	void DeInit()
	{
		CloseWindow();
	}

	void run(bool& exitGame)
	{
		bool exitMatch = true;

		//Main menu and variable set
		InitMatch(exitMatch, exitGame);

		// Main match loop
		while (!exitMatch && !WiningScoreReached())    // Detect window close button or ESC key
		{
			Input(exitMatch);

			Update();

			Output();
		}
	}
}
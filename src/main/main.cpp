#include "..\..\src\gameplay\include\gameplay.h"

using namespace GAME;
void main()
{    
    bool exitGame = false;

    InitGame();
    
    // Main game loop
    do
    {
        run(exitGame);
        
    } while (!exitGame);    

    DeInit();
}